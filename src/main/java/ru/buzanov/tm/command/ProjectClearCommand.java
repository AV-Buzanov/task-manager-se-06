package ru.buzanov.tm.command;

import ru.buzanov.tm.entity.Project;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getUser().getId();
        for (Project project : bootstrap.getProjectService().findAll(bootstrap.getUser().getId()))
            bootstrap.getTaskService().removeByProjectId(userId, project.getId());
        bootstrap.getProjectService().removeAll(bootstrap.getUser().getId());
        System.out.println("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
