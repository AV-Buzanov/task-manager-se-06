package ru.buzanov.tm.command;

public class ExitCommand extends AbstractCommand {
    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        StringBuilder s = new StringBuilder("[GOODBYE! TILL WE MEET AGAIN!]");
        if (bootstrap.getUser()!=null)
            s.insert(8, ", ").insert(10, bootstrap.getUser().getName());
        System.out.println(s);
        System.exit(0);
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
