package ru.buzanov.tm.command;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;

public class ProjectViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "View project information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO VIEW]");
        String userId = bootstrap.getUser().getId();
        System.out.println(bootstrap.getProjectService().getList(userId));
        String idBuf = bootstrap.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Project projectBuf = bootstrap.getProjectService().findOne(userId, idBuf);
        System.out.println("[NAME] ");
        System.out.println(projectBuf.getName());
        System.out.println("[START DATE] ");
        if (projectBuf.getStartDate() != null)
            System.out.println(DateUtil.dateFormat().format(projectBuf.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[END DATE] ");
        if (projectBuf.getEndDate() != null)
            System.out.println(DateUtil.dateFormat().format(projectBuf.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[DESCRIPTION] ");
        if (projectBuf.getDescription() != null)
            System.out.println(projectBuf.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[TASKS]");
        if (bootstrap.getTaskService().findByProjectId(userId, idBuf).isEmpty()) {
            System.out.println(FormatConst.EMPTY_FIELD);
            return;
        }
        for (Task task : bootstrap.getTaskService().findByProjectId(userId, idBuf)) {
            System.out.println(task.getName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
