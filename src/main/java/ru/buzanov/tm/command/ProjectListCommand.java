package ru.buzanov.tm.command;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println(bootstrap.getProjectService().getList(bootstrap.getUser().getId()));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
