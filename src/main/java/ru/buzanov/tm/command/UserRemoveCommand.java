package ru.buzanov.tm.command;

import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

public class UserRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-remove";
    }

    @Override
    public String description() {
        return "Remove selected user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE USER TO REMOVE]");
        User[] users = new User[bootstrap.getUserService().findByRole(RoleType.USER).size()];
        bootstrap.getUserService().findByRole(RoleType.USER).toArray(users);
        for (int i=0;i<users.length;i++){
            System.out.print(i+1);
            System.out.println(": "+users[i].getLogin()+" "+users[i].getName());
        }
        int indexBuf = Integer.parseInt(reader.readLine())-1;
        System.out.println("[ARE YOU SURE(Y/N)?]");
        if ("Y".equals(reader.readLine())){
            bootstrap.getUserService().remove(users[indexBuf].getId());
            bootstrap.getProjectService().removeAll(users[indexBuf].getId());
            bootstrap.getTaskService().removeAll(users[indexBuf].getId());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.USER.equals(role))
            return false;
        return super.isRoleAllow(role);
    }
}
