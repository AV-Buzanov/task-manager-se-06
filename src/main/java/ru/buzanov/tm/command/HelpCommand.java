package ru.buzanov.tm.command;

public class HelpCommand extends AbstractCommand{

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command:bootstrap.getCommands()){
            System.out.println(command.command()+": "+command.description());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
