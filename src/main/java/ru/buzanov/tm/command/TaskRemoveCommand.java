package ru.buzanov.tm.command;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE TASK TO REMOVE]");
        String userId = bootstrap.getUser().getId();
        System.out.println(bootstrap.getTaskService().getList(userId));
        String idBuf = bootstrap.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        bootstrap.getTaskService().remove(userId, idBuf);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
