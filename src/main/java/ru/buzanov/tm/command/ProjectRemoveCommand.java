package ru.buzanov.tm.command;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO REMOVE]");
        String userId = bootstrap.getUser().getId();
        System.out.println(bootstrap.getProjectService().getList(userId));
        String idBuf = bootstrap.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        bootstrap.getProjectService().remove(userId, idBuf);
        bootstrap.getTaskService().removeByProjectId(userId, idBuf);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
