package ru.buzanov.tm.command;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        Task task = new Task();
        String userId = bootstrap.getUser().getId();
        String stringBuf;
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            System.out.println("Task name can't be empty");
            return;
        }
        if (bootstrap.getTaskService().isNameExist(userId, stringBuf)) {
            System.out.println("Task with this name already exist.");
            return;
        }
        task.setName(stringBuf);
        System.out.println("[ENTER TASK START DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[ENTER TASK END DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setDescription(stringBuf);
        if (!bootstrap.getProjectService().findAll(userId).isEmpty()) {
            System.out.println("[CHOOSE PROJECT]");
            System.out.println(bootstrap.getProjectService().getList(userId));
            stringBuf = reader.readLine();
            if (!stringBuf.isEmpty()) {
                task.setProjectId(bootstrap.getProjectService().getIdByCount(userId, Integer.parseInt(stringBuf)));
            }
        }
        task.setUserId(userId);
        bootstrap.getTaskService().load(task);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
