package ru.buzanov.tm.command;

public class UserLogOutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "log-out";
    }

    @Override
    public String description() {
        return "User log-out";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setUser(null);
        System.out.println("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
