package ru.buzanov.tm.command;

public class UserViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "View user information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VIEW USER]");
        String userId = bootstrap.getUser().getId();
        System.out.println("[LOGIN]");
        System.out.println(bootstrap.getUser().getLogin());
        System.out.println("[NAME]");
        System.out.println(bootstrap.getUser().getName());
        System.out.println("[ROLE]");
        System.out.println(bootstrap.getUser().getRoleType().displayName());
        System.out.println("[PROJECTS COUNT]");
        System.out.println(bootstrap.getProjectService().findAll(userId).size());
        System.out.println("[TASKS COUNT]");
        System.out.println(bootstrap.getTaskService().findAll(userId).size());
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
