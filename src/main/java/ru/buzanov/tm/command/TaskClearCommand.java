package ru.buzanov.tm.command;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        String userId = bootstrap.getUser().getId();
        bootstrap.getTaskService().removeAll(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
