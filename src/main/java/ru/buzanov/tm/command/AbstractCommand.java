package ru.buzanov.tm.command;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure() throws Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }

}
