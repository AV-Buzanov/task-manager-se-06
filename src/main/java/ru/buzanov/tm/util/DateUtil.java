package ru.buzanov.tm.util;

import ru.buzanov.tm.constant.FormatConst;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateUtil {
    private final static SimpleDateFormat df = new SimpleDateFormat(FormatConst.DATE_FORMAT, Locale.ENGLISH);
    public static SimpleDateFormat dateFormat(){
        return df;
    }
}
