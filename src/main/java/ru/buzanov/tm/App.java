package ru.buzanov.tm;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.command.ExitCommand;
import ru.buzanov.tm.command.HelpCommand;
import ru.buzanov.tm.command.ProjectClearCommand;

/**
 * Task Manager
 */

public class App {
    public static void main(String[] args) {
        Class[] commandClasses = new Class[]{ExitCommand.class, HelpCommand.class, ProjectClearCommand.class};
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}