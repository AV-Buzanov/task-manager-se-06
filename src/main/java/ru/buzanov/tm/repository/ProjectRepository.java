package ru.buzanov.tm.repository;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {
    private Bootstrap bootstrap;
    private Map<String, Project> projects = new LinkedHashMap<>();

    public ProjectRepository() {
    }

    public ProjectRepository(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Project persist() {
        Project project = new Project();
        projects.put(project.getId(), project);
        return project;
    }

    public Project persist(String userId) {
        Project project = new Project();
        project.setUserId(userId);
        projects.put(project.getId(), project);
        return project;
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Collection<Project> findAll(String userId) {
        Collection<Project> list = new ArrayList<>();
        for (Project project : projects.values()) {
            if (userId.equals(project.getUserId()))
                list.add(project);
        }
        return list;
    }

    public Project findOne(String id) {
        if (projects.containsKey(id))
            return projects.get(id);
        else return null;
    }

    public Project findOne(String userId, String id) {
        if (projects.containsKey(id)&&userId.equals(projects.get(id).getUserId()))
            return projects.get(id);
        else return null;
    }

    public boolean isNameExist(String userId, String name) {
        for (Project project : findAll(userId)) {
            if (name.equals(project.getName()))
                return true;
        }
        return false;
    }

    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (Project project : findAll()) {
            s.append(indexBuf).append(". ").append(project.getName());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getList(String userId) {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (Project project : findAll(userId)) {
            s.append(indexBuf).append(". ").append(project.getName());
            if (findAll(userId).size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getIdByCount(int count) {
        int indexBuf = 1;
        for (Project project : findAll()) {
            if (indexBuf == count)
                return project.getId();
            indexBuf++;
        }
        return null;
    }

    public String getIdByCount(String userId, int count) {
        int indexBuf = 1;
        for (Project project : findAll(userId)) {
            if (indexBuf == count)
                return project.getId();
            indexBuf++;
        }
        return null;
    }

    public void load(Project project) {
        projects.put(project.getId(),project);
    }

    public void merge(String id, Project project) {
        if (project.getName() != null)
            projects.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            projects.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            projects.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            projects.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            projects.get(id).setUserId(project.getUserId());
    }

    public void merge(String userId, String id, Project project) {
        if (!userId.equals(projects.get(id).getUserId()))
            return;
        if (project.getName() != null)
            projects.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            projects.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            projects.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            projects.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            projects.get(id).setUserId(project.getUserId());
    }

    public Project remove(String id) {
        return projects.remove(id);
    }

    public Project remove(String userId, String id) {
        if (userId.equals(projects.get(id).getUserId()))
        return projects.remove(id);
        return null;
    }

    public void removeAll() {
        for (Project project : findAll())
            remove(project.getId());
    }

    public void removeAll(String userId) {
        for (Project project : findAll(userId))
            remove(project.getId());
    }
}