package ru.buzanov.tm.repository;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Task;

import java.util.*;


public class TaskRepository {
    private Bootstrap bootstrap;
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public TaskRepository() {
    }

    public TaskRepository(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Task persist() {
        Task task = new Task();
        tasks.put(task.getId(), task);
        return task;
    }

    public Task persist(String userId) {
        Task task = new Task();
        task.setUserId(userId);
        tasks.put(task.getId(), task);
        return task;
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Collection<Task> findAll(String userId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (userId.equals(task.getUserId()))
                list.add(task);
        }
        return list;
    }

    public Task findOne(String id) {
        if (tasks.containsKey(id))
            return tasks.get(id);
        else return null;
    }

    public Task findOne(String userId, String id) {
        if (tasks.containsKey(id)&&userId.equals(tasks.get(id).getUserId()))
            return tasks.get(id);
        else return null;
    }

    public boolean isNameExist(String userId, String name) {
        for (Task task : findAll(userId)) {
            if (name.equals(task.getName()))
                return true;
        }
        return false;
    }

    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (Task task : findAll()) {
            s.append(indexBuf).append(". ").append(task.getName());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getList(String userId) {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (Task task : findAll(userId)) {
            s.append(indexBuf).append(". ").append(task.getName());
            if (findAll(userId).size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getIdByCount(int count) {
        int indexBuf = 1;
        for (Task task : findAll()) {
            if (indexBuf == count)
                return task.getId();
            indexBuf++;
        }
        return null;
    }

    public String getIdByCount(String userId, int count) {
        int indexBuf = 1;
        for (Task task : findAll(userId)) {
            if (indexBuf == count)
                return task.getId();
            indexBuf++;
        }
        return null;
    }

    public void load(Task task) {
        tasks.put(task.getId(),task);
    }

    public void merge(String id, Task task) {
        if (task.getName() != null)
            tasks.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            tasks.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            tasks.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            tasks.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            tasks.get(id).setUserId(task.getUserId());
    }

    public void merge(String userId, String id, Task task) {
        if (!userId.equals(tasks.get(id).getUserId()))
            return;
        if (task.getName() != null)
            tasks.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            tasks.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            tasks.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            tasks.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            tasks.get(id).setUserId(task.getUserId());
    }

    public Task remove(String id) {
        return tasks.remove(id);
    }

    public Task remove(String userId, String id) {
        if (userId.equals(tasks.get(id).getUserId()))
            return tasks.remove(id);
        return null;
    }

    public void removeAll() {
        for (Task task : findAll())
            remove(task.getId());
    }

    public void removeAll(String userId) {
        for (Task task : findAll(userId))
            remove(task.getId());
    }
    
    public Collection<Task> findByProjectId(String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(String projectId) {
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }

    public Collection<Task> findByProjectId(String userId, String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(String userId, String projectId) {
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }
}
