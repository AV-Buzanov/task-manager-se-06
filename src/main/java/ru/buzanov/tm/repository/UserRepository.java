package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {
    private Map<String, User> users = new LinkedHashMap<>();

    public User persist() {
        User user = new User();
        users.put(user.getId(), user);
        return user;
    }

    public Collection<User> findAll() {
        return users.values();
    }

    public User findOne(String id) {
        if (users.containsKey(id))
            return users.get(id);
        else return null;
    }

    public User findByLogin(String login) {
        if (isLoginExist(login)) {
            for (User user : findAll()) {
                if (user.getLogin().equals(login))
                    return user;
            }
        }
        return null;
    }

    public Collection<User> findByRole(RoleType role) {
        Collection<User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getRoleType().equals(role))
                users.add(user);
        }
        return users;
    }

    public boolean isPassCorrect(String login, String pass) {
        return pass.equals(findByLogin(login).getPasswordHash());
    }

    public boolean isLoginExist(String login) {
        for (User user : findAll()) {
            if (user.getLogin().equals(login))
                return true;
        }
        return false;
    }

    public void merge(String id, User user) {
        if (user.getLogin() != null)
            users.get(id).setLogin(user.getLogin());
        if (user.getPasswordHash() != null)
            users.get(id).setPasswordHash(user.getPasswordHash());
        if (user.getRoleType() != null)
            users.get(id).setRoleType(user.getRoleType());
        if (user.getName() != null)
            users.get(id).setName(user.getName());
    }

    public User remove(String id) {
        return users.remove(id);
    }

    public void removeAll() {
        users.clear();
    }
}
