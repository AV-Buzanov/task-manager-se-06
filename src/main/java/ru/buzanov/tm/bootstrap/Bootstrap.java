package ru.buzanov.tm.bootstrap;

import org.reflections.Reflections;
import ru.buzanov.tm.command.*;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Bootstrap {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final ProjectRepository projectRepository = new ProjectRepository(this);
    private final TaskRepository taskRepository = new TaskRepository(this);
    private final ProjectService projectService = new ProjectService(this);
    private final TaskService taskService = new TaskService(this);
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private User user;
    private Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.buzanov.tm").getSubTypesOf(AbstractCommand.class);


    private void init() throws IllegalAccessException, InstantiationException {
        for (Class clas : classes) {
            registryCommand((AbstractCommand) clas.newInstance());
        }
        registryUser(new User("UserName", "user", "123456", RoleType.USER));
        registryUser(new User("AdminName", "admin", "123456", RoleType.ADMIN));
    }

    private void registryCommand(final AbstractCommand command) {
        if (command.command() == null || command.command().isEmpty()) {
            return;
        }
        if (command.description() == null || command.description().isEmpty()) {
            return;
        }
        command.setBootstrap(this);
        commands.put(command.command(), command);
    }

    private void registryUser(final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            return;
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) {
            return;
        }
        if (user.getName() == null || user.getName().isEmpty()) {
            return;
        }
        if (user.getRoleType() == null) {
            return;
        }
        userService.merge(userService.persist().getId(), user);
    }

    public void start() {
        try {
            init();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        String command = "";

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (!commands.get("exit").command().equals(command)) {
            try {
                command = reader.readLine();
                if (!command.isEmpty()) {

                    if (user == null && commands.get(command).isSecure())
                        System.out.println("Authorise, please");
                    else {
                        if (user != null && !commands.get(command).isRoleAllow(user.getRoleType()))
                            System.out.println("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }

                }

            } catch (ParseException e) {
                System.out.println("Wrong input, try again");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selected index doesn't exist");
            } catch (Exception e) {
                System.out.println("Something wrong, try again");
            }
        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public UserService getUserService() {
        return userService;
    }

    public User getUser() {
        return user;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}