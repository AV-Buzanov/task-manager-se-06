package ru.buzanov.tm.service;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.Collection;

public class ProjectService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private Bootstrap bootstrap;

    public ProjectService() {
    }

    public ProjectService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.taskRepository = bootstrap.getTaskRepository();
        this.projectRepository = bootstrap.getProjectRepository();
    }

    public Project persist() {
        return projectRepository.persist();
    }

    public Project persist(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return projectRepository.persist(userId);
    }

    public Collection<Project> findAllAdmin() {
        return projectRepository.findAll();
    }

    public Collection<Project> findAll(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return projectRepository.findAll(userId);
    }

    public Project findOneAdmin(String id) {
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.findOne(id);
    }

    public Project findOne(String userId, String id) {
        if (userId==null||userId.isEmpty())
            return null;
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.findOne(userId, id);
    }

    public boolean isNameExist(String userId, String name) {
        if (userId==null||userId.isEmpty())
            return false;
        if (name==null||name.isEmpty())
            return false;
        return projectRepository.isNameExist(userId, name);
    }

    public String getListAdmin() {
        return projectRepository.getList();
    }

    public String getList(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return projectRepository.getList(userId);
    }

    public String getIdByCountAdmin(int count) {
        return projectRepository.getIdByCount(count);
    }

    public String getIdByCount(String userId, int count) {
        if (userId==null||userId.isEmpty())
            return null;
        return projectRepository.getIdByCount(userId, count);
    }

    public void mergeAdmin(String id, Project project) {
        if (id==null||id.isEmpty())
            return;
        if (project==null)
            return;
        projectRepository.merge(id, project);
    }

    public void merge(String userId, String id, Project project) {
        if (userId==null||userId.isEmpty())
            return;
        if (id==null||id.isEmpty())
            return;
        if (project==null)
            return;
        projectRepository.merge(userId, id, project);
    }

    public Project removeAdmin(String id) {
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.remove(id);
    }

    public Project remove(String userId, String id) {
        if (userId==null||userId.isEmpty())
            return null;
        if (id==null||id.isEmpty())
            return null;
        return projectRepository.remove(userId, id);
    }

    public void removeAllAdmin() {
        projectRepository.removeAll();
    }

    public void removeAll(String userId) {
        if (userId==null||userId.isEmpty())
            return;
        projectRepository.removeAll(userId);
    }

    public boolean load(Project project) {
        if (project==null)
            return false;
        if (project.getUserId()==null||project.getUserId().isEmpty())
            return false;
        if (project.getId()==null||project.getId().isEmpty())
            return false;
        projectRepository.load(project);
        return true;
    }
}