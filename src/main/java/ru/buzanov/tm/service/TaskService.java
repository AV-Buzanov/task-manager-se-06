package ru.buzanov.tm.service;

import ru.buzanov.tm.bootstrap.Bootstrap;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.Collection;

public class TaskService {

    private Bootstrap bootstrap;
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;

    public TaskService() {
    }

    public TaskService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.taskRepository = bootstrap.getTaskRepository();
        this.projectRepository = bootstrap.getProjectRepository();
    }

    public Task persistAdmin() {
        return taskRepository.persist();
    }

    public Task persist(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return taskRepository.persist(userId);
    }

    public Collection<Task> findAllAdmin() {
        return taskRepository.findAll();
    }

    public Collection<Task> findAll(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return taskRepository.findAll(userId);
    }

    public Task findOneAdmin(String id) {
        if (id==null||id.isEmpty())
            return null;
        return taskRepository.findOne(id);
    }

    public Task findOne(String userId, String id) {
        if (userId==null||userId.isEmpty())
            return null;
        if (id==null||id.isEmpty())
            return null;
        return taskRepository.findOne(userId, id);
    }

    public boolean isNameExist(String userId, String name) {
        if (userId==null||userId.isEmpty())
            return false;
        if (name==null||name.isEmpty())
            return false;
        return taskRepository.isNameExist(userId, name);
    }

    public String getListAdmin() {
        return taskRepository.getList();
    }

    public String getList(String userId) {
        if (userId==null||userId.isEmpty())
            return null;
        return taskRepository.getList(userId);
    }

    public String getIdByCountAdmin(int count) {
        return taskRepository.getIdByCount(count);
    }

    public String getIdByCount(String userId, int count) {
        if (userId==null||userId.isEmpty())
            return null;
        return taskRepository.getIdByCount(userId, count);
    }

    public void load(Task task) {
        if (task==null)
            return;
        taskRepository.load(task);
    }

    public void mergeAdmin(String id, Task task) {
        if (id==null||id.isEmpty())
            return;
        if (task==null)
            return;
        taskRepository.merge(id, task);
    }

    public void merge(String userId, String id, Task task) {
        if (userId==null||userId.isEmpty())
            return;
        if (id==null||id.isEmpty())
            return;
        if (task==null)
            return;
        taskRepository.merge(userId, id, task);
    }

    public Task removeAdmin(String id) {
        if (id==null||id.isEmpty())
            return null;
        return taskRepository.remove(id);
    }

    public Task remove(String userId, String id) {
        if (id==null||id.isEmpty())
            return null;
        if (userId==null||userId.isEmpty())
            return null;
        return taskRepository.remove(userId, id);
    }

    public void removeAllAdmin() {
        taskRepository.removeAll();
    }

    public void removeAll(String userId) {
        if (userId==null||userId.isEmpty())
            return;
        taskRepository.removeAll(userId);
    }

    public Collection<Task> findByProjectIdAdmin(String projectId) {
        if (projectId==null||projectId.isEmpty())
            return null;
        return taskRepository.findByProjectId(projectId);
    }

    public void removeByProjectIdAdmin(String projectId) {
        if (projectId==null||projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(projectId);
    }

    public Collection<Task> findByProjectId(String userId, String projectId) {
        if (userId==null||userId.isEmpty())
            return null;
        if (projectId==null||projectId.isEmpty())
            return null;
        return taskRepository.findByProjectId(userId, projectId);
    }

    public void removeByProjectId(String userId, String projectId) {
        if (userId==null||userId.isEmpty())
            return;
        if (projectId==null||projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(userId, projectId);
    }
}