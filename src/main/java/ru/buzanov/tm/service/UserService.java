package ru.buzanov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.UserRepository;

import java.util.Collection;

public class UserService {
    private UserRepository userRepository;

    public UserService() {
    }

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User persist() {
        return userRepository.persist();
    }

    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String id) {
        if (id==null||id.isEmpty())
            return null;
        return userRepository.findOne(id);
    }

    public void merge(String id, User user) {
        if (id==null||user==null||id.isEmpty())
            return;
        user.setPasswordHash(DigestUtils.md5Hex(user.getPasswordHash()));
        userRepository.merge(id, user);
    }

    public User remove(String id) {
        if (id==null||id.isEmpty())
            return null;
        return userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public User findByLogin(String login) {
        if (login==null||login.isEmpty())
            return null;
        return userRepository.findByLogin(login);
    }

    public boolean isPassCorrect(String login, String pass) {
        if (login==null||login.isEmpty())
            return false;
        if (pass==null||pass.isEmpty())
            return false;
        return userRepository.isPassCorrect(login, DigestUtils.md5Hex(pass));
    }

    public boolean isLoginExist(String login) {
        if (login==null||login.isEmpty())
            return false;
        return userRepository.isLoginExist(login);
    }

    public Collection<User> findByRole(RoleType role) {
        if (role==null)
            return null;
        return userRepository.findByRole(role);
    }
}